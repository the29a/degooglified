# Список сервисов и продуктов, которые могут быть альтернативой сервисов Google.
Данный список - попытка перечислить возможные альтернативы продуктам Google.
Возможно, не все из них удобны, или могут потребовать усилий в освоении.
Список частично основывается на статье из [Википедии](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%81%D0%BB%D1%83%D0%B6%D0%B1_%D0%B8_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%BE%D0%B2_Google)

# Оглавление

- [Замена продуктам Google](#Замена-продуктам-Google)
  - [Веб-продкты](#Веб-продукты)
  - [Публичный DNS](#Публичный-DNS)
  - [Операционные системы](#Операционные-системы)
  - [Десктопные приложения](#Десктопные-приложения)
  - [Мобильные приложения](#Мобильные-приложения)
  - [Аппаратные средства](#Аппаратные-средства)
- [Рекомендации](#Рекомендации)
- [Заключение](#Заключение)
- [To-do](#To-do)
- [Лицензия](#Лицензия)

# Замена продуктам Google
## Веб-продукты
В предисловии хотелось бы добавить, что часть сервисов в виде облачного хранилища, каледарей или заметок можно заменить owncloud, nextcloud и FreedomBox.

- Google Search
  - [DuckDuckGo.com](https://duckduckgo.com/) - Поисковая система, ориентированная на конфиденциальность, которая не сохраняет куки и не отслеживает вас или ваши поисковые запросы.
  - [StartPage.com](https://startpage.com/) - Еще один поисковик, ориентированный на конфиденциальность.
  Хоть он и улучшен силами Google, своих пользователей он не отслеживает. Кроме того, у него есть веб-прокси.
  - [searx.me](https://searx.me/) - отличная поисковая система, ориентированная на конфиденциальность. Отличный функционал и интерфейс.
  - [Qwant.com](https://www.qwant.com) - довольно интересный поисковик. Из минусов определенная ограниченость в поиске.

- GMail
  - [Protonmail.com](https://protonmail.com/) - известный почтовый сервис от создателей ProtonVPN. Имеет бесплантный (с ограничениями) и платные варианты использования.
  - [tutanota.com](https://tutanota.com/) - не менее известный сервис. Так же имеет беспланый и платные планы использования. Кроме того, исходный код сервиса в открытом доступе.

- Translate
  - [dict.cc](https://www.dict.cc/) - переводчик, поддерживающий многие языки. Но из отзывов, есть проблемы с работой самого сайта.
  - [linguee.com](https://www.linguee.com/) - переводчик с ограниченым колличеством языков, но есть расширенный поиск перевода.
  - [deepl.com](https://www.deepl.com/translator) - интересный сервис, который использует в переводе машинное обучение. Языков не так много, но авторы обещают, что сам перевод будет качественный.

- G Suite
  - [zoho.com](https://www.zoho.com/) - Сервис бизнес приложений, включающий почту, CRM, возможность разничных интеграций (в т.ч. с IT-отделами). Не тестировался.

- Blogger
  - [telegra.ph](telegra.ph) - удобная платформа для публикации статей без регистрации и смс. Из минусов - блокировка на территории России (такая себе блокировка).
  - [medium.com](https://medium.com/about) - известная многим платформа, с множеством интересных публикаций. В России она medium обрел особую популярность после блокировки telegra.ph
  - [wix.com](wix.com) - больше конструктор сайтов с хостингом, чем блог-площадка. Есть как бесплатные тарифы, так премиум-тарифы.
  - [wordpress.com](https://wordpress.com/) - известная площадка и движок для сайтов. Имеется возможность использовать движок на своем хостинге, или же использовать ресурсы wordpress.com. В наличии бесплатный и платные тарифы.

- Google Calendar
    - [fruux.com](https://fruux.com/) - хороший проект с открытым исходным кодом. Содержит в себе не только календарь, но задачи и контакты, что может быть удобно для комманд.

- Google Docs
  - [Zoho Docs](https://www.zoho.com/docs/)
  - [OnlyOffice](https://personal.onlyoffice.com/ru/)
  - [LibreOffice](https://www.libreoffice.org/) или [OpenOffice](https://www.openoffice.org/ru/) - если не требуется онлайн-редактирования и размещения в облаке.

- Google Drive
  - [mega.nz](https://mega.nz/) - отличное облачное хранилище от создателя Megaupload - Кима Доткома. На бесплатном тарифе дает от 15ГБ, но может быть расширен с помощью разных бонусов. Есть и платные тарифы на неплохих условиях.
  - [syncthing.net](https://syncthing.net/) - распределенное хранилище, которое ближе к Bittorrent, чем к привычным "облакам". Проект с открытым исходным кодом.
  - [owndrive](https://owndrive.com/)
  - [Nextcloud](nextcloud.com) или [OwnCloud](owncloud.org) - вариант для самостоятельных. Требует установки на свой сервер или VPS\VDS.

- Google Photos  
  - [piwigo.org](https://piwigo.org/) - онлайн-галерея с открытым исходным кодом. Как сервис не предоставляется, необходимо использовать хостинг или VPS/VDS.

- Google Domains
  - [namecheap.com](https://www.namecheap.com)
  - [name.com](https://www.name.com)

- Google Keep
  - [joplin.cozic.net](https://joplin.cozic.net/)
  - [evernote.com](https://evernote.com/)

- Play Music
  - [spotify.com](https://spotify.com/)
  - [last.fm](https://www.last.fm/)
  - [soundcloud.com](https://soundcloud.com/)
  - [bandcamp.com](https://bandcamp.com)

- Play Movies and TV
  - [netflix.com](https://netflix.com/)
  - [hulu.com](http://www.hulu.com/)

- News
  - тут ничего нет.

- Google Finance
  - тут ничего нет.

- Google Hangouts
  - [Wire](https://wire.com/en/)

- Google+
  - не актуально, сервис закрывается

- Google Sites
  - [weebly.com](https://www.weebly.com/)
  - [squarespace.com](https://www.squarespace.com/)

- Google Fonts
  - [fontsquirrel.com](https://www.fontsquirrel.com/)
  - [fontlibrary.org](https://fontlibrary.org/)
  - [dafont.com](https://www.dafont.com/)

- YouTube (как сайт)
  - [hooktube.com](https://hooktube.com/)
  - [vimeo.com](https://vimeo.com/)

- YouTube (приложение)
  - [NewPipe](https://newpipe.schabi.org/)

- Google Classroom
  - [moodle.org](https://moodle.org/)
  - [canvaslms.com](https://www.canvaslms.com/?lead_source_description=instructure.com_)

- Google Maps/Transit
  - [openstreetmap.org](https://www.openstreetmap.org/)
  - [HERE WeGo](https://wego.here.com/)

- Google Trends
  - -
   
  
## Публичный DNS
- Google Public DNS
   - Альтернатив множество, например [мой список](https://gitlab.com/the29a/dns-list)

## Операционные системы
- Android
  - [AOSP Extended](https://aospextended.com/)
  - [LineageOS](https://www.lineageos.org/)
  - [CopperheadOS](https://copperhead.co/android/)

- Chrome OS (для хромбуков)
  - [GaliumOS](https://galliumos.org/)
  - [Void Linux](https://www.voidlinux.eu)
  - [Bliss OS](https://blissroms.com/)

- Wear OS
  - [AsteroidOS](https://asteroidos.org/#)

- Android Auto
  - - 

- Android TV
  - [Kodi](https://kodi.tv/)

## Десктопные приложения
- Google Chrome 
  - [Firefox](https://www.mozilla.org/en-US/firefox/new/)
  - [Brave](https://brave.com/)
  - [Opera](https://www.opera.com/)
  - [Vivaldi](https://vivaldi.com/ru/)
  - [Brave](https://brave.com/)
  - [Ungoogled Chromium](https://ungoogled-software.github.io/ungoogled-chromium-binaries)
  - [Tor](https://www.torproject.org/download/download-easy.html.en)

- Google Earth
  - - 

- Android Studio
  - [IntelliJ IDEA](https://www.jetbrains.com/idea/)

## Мобильные приложения
- Play Store
  - [F-Store](https://f-droid.org/)
  - [Yalp Store](https://github.com/yeriomin/YalpStore)
  - [APKMirror](https://www.apkmirror.com/)

- Maps
  - [Magic Earth](https://www.generalmagic.com/magic-earth)

- Google News
  - -

- Google Pay
  - Система закрытая, из аналогов Samsung Pay.

- YouTube для Android
  - [NewPipe](https://newpipe.schabi.org/)

- Google Authenticator
  - [Authy](https://authy.com/)
  - [FreeOTP](https://freeotp.github.io/)
  - [LastPass Authenticator](https://lastpass.com/auth/)
  - [AndOTP](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp)
 
- Файловый менеджер
  - [Amaze File Manager](https://github.com/TeamAmaze/AmazeFileManager) 

- Gboard
  - [AnySoftKeyboard](https://anysoftkeyboard.github.io/)
  - [Hacker's Keyboard](https://play.google.com/store/apps/details?id=org.pocketworkstation.pckeyboard)

## Аппаратные средства
- Смартфоны Nexus или Pixel
  - Использовать кастомные прошивки (см. Операционные системы)

- Chromebook
  - Использовать с альтернативными ОС (см. Операционные системы)
  - Не самый плохой вариант вместо хромбука использовать refubrished ноутбуки Lenovo Thinkpad (например x201 или x220) или иной ноутбук корп. уровня.

- Chromecast
  - Устройства от [Roku](https://www.roku.com/en-gb/)
  - [Kodi](https://kodi.tv/)

- Google Home
  - У устройств такого типа схожий функционал.  
-----------------
# To-do list:
- [ ] Дополнить список
- [ ] Дополнить описание
-----------------
# Лицензия 
Degooglified распространяется под лицензией DBAD, детали лицензии смотрите в [LICENSE.md](LICENSE.md)